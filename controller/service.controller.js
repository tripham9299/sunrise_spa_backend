var serviceModel = require('../models/service.model')
const cloudinary = require('cloudinary').v2
const streamifier = require('streamifier')


let controller = {}

module.exports = controller
controller.index= async (req,res)=>{
	try{
		let service= await serviceModel.find();
		res.json(service);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}

controller.serviceCreate = async (req,res,next)=>{

	let streamUpload = (req) => {
    return new Promise((resolve, reject) => {
        let stream = cloudinary.uploader.upload_stream(
          (error, result) => {
            if (result) {
              resolve(result);
            } else {
              reject(error);
            }
          }
        );

       		streamifier.createReadStream(req.file.buffer).pipe(stream);
    	});
	};

	try {
		let result = await streamUpload(req);
		let newService = await serviceModel.create({name:req.body.name,price:req.body.price,description:req.body.description,img:result.secure_url});
		res.json(newService);
	}
	catch (err) {
		console.log(err)
		res.status(500).json({ error: err })
	}
}
controller.updateService= async (req,res) =>{

	try{
		let id = req.params.id;
		if(req.file){
			let streamUpload = (req) => {
			    return new Promise((resolve, reject) => {
			        let stream = cloudinary.uploader.upload_stream(
			          (error, result) => {
			            if (result) {
			              resolve(result);
			            } else {
			              reject(error);
			            }
			          }
			        );

			       		streamifier.createReadStream(req.file.buffer).pipe(stream);
			    	});
				};
			let result = await streamUpload(req);
			let updateService= await serviceModel.findOneAndUpdate({_id:id},
				{name:req.body.service,price:req.body.price,description:req.body.description,img:result.secure_url},
				{new:true})
			res.json(updateService);
		}
		else{
			let updateService= await serviceModel.findOneAndUpdate({_id:id},{name:req.body.service,price:req.body.price,description:req.body.description},{new:true})
			res.json(updateService);
		}
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}
controller.deleteService= async (req,res) =>{
	try{
		let id = req.params.id;
		let deleteService= await serviceModel.remove({_id:id})
		res.json(deleteService);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}