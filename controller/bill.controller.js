var billModel = require('../models/bill.model')

let controller = {}

module.exports = controller

controller.index= async(req,res)=>{
	try{
		let bill= await billModel.find().populate('service');
		res.json(bill);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}

controller.billUser = async(req,res)=>{
	try{
		let billUser = await billModel.find({user:req.user.id}).populate('service');
		res.json(billUser);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}

controller.newBill= async(req,res)=>{
	try{
		let id = req.params.id;
		let newBill= await billModel.create({user:req.user.id,userName:req.body.userName,phone:req.body.phone,usedTime:req.body.usedTime,totalMoney:req.body.totalMoney,service:id});
		res.json(newBill);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}

controller.deleteBill= async(req,res)=>{
	try{
		let id = req.params.id;
		let deleteBill= await billModel.remove({_id:id})
		res.json(deleteBill);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}
controller.updateStatus= async(req,res)=>{
	console.log(req.params.id)
	try{
		let id = req.params.id;
		let updateStatus= await billModel.updateOne({_id:id},{status:1});
		res.json(updateStatus);
	}
	catch(err){
		console.log(err)
		res.status(500).json({ error: err })
	}
}
