var express = require('express');
var router = express.Router();
var multer  = require('multer');
var controller = require('../controller/bill.controller');
var authMiddleware = require('../middleware/auth.middleware')
var upload = multer({ dest: './public/uploads/' });
var authMiddleware = require('../middleware/auth.middleware')
router.get('/',controller.index)

router.get('/billUser',authMiddleware.isAuth,controller.billUser);

router.post('/newBill/:id',authMiddleware.isAuth,
  controller.newBill
);

router.put('/updateStatus/:id',controller.updateStatus);

router.delete('/deleteBill/:id',authMiddleware.isAuth,
  controller.deleteBill
);


module.exports = router;
